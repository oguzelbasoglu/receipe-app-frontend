import React, { useState, useEffect } from "react";
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Button,
  FlatList,
  ActivityIndicator,
} from "react-native";
import * as Linking from "expo-linking";

const Login = () => {
  //FONKSİYONA ÇEVİR
  return (
    <View>
      <Image
        style={{
          resizeMode: "cover",
          top: 200,
          left: 0,
        }}
        source={require("./assets/logo.png")}
      ></Image>
      <TouchableOpacity
        style={{
          top: 400,
          left: 0,
        }}
      >
        <Image
          style={{
            resizeMode: "cover",
            height: 60,
            width: 60,
          }}
          source={require("./assets/ppbeyaz.png")}
        ></Image>
      </TouchableOpacity>
      <View
        style={{
          top: 420,
          left: 0,
        }}
      >
        <TouchableOpacity>
          <Text
            style={{
              fontSize: 20,
            }}
          >
            Giriş yapmadan devam et
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={{
          top: 180,
          left: 0,
        }}
      >
        <Image
          style={{
            resizeMode: "cover",
            height: 60,
            width: 60,
          }}
          source={require("./assets/google.png")}
        ></Image>
      </TouchableOpacity>
      <View
        style={{
          top: 200,
          left: 0,
        }}
      >
        <Text
          style={{
            fontSize: 20,
          }}
        >
          Giriş yap
        </Text>
      </View>
    </View>
  );
};

//Screens

const MenuLogged = () => {
  return (
    <View style={{ flex: 1 }}>
      <TopBar></TopBar>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/tarifler.png")}
        />
        <Text>Tarifler</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/ara.png")}
        />
        <Text>Ara</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/bug.png")}
        />
        <Text>Hata Bildir</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/hakkında.png")}
        />
        <Text>Hakkında</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-around",
            alignContent: "space-between",
          }}
          source={require("./assets/çıkış.png")}
        />
        <Text>Çıkış Yap</Text>
      </TouchableOpacity>
      <BotBar></BotBar>
      <Buttons></Buttons>
    </View>
  );
};

const MenuNotLogged = () => {
  return (
    <View style={{ flex: 1 }}>
      <TopBar></TopBar>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 1,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/giriş.png")}
        />
        <Text>Giriş Yap</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/tarifler.png")}
        />
        <Text>Tarifler</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/ara.png")}
        />
        <Text>Ara</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/bug.png")}
        />
        <Text>Hata Bildir</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          style={{
            height: 50,
            width: 50,
            flex: 0,
            justifyContent: "space-between",
            alignContent: "space-between",
          }}
          source={require("./assets/hakkında.png")}
        />
        <Text>Hakkında</Text>
      </TouchableOpacity>
      <BotBar></BotBar>
      <Buttons></Buttons>
    </View>
  );
};

const ReportError = () => {
  return (
    <View style={{ flex: 1 }}>
      <TopBar></TopBar>
      <BotBar></BotBar>
      <Buttons></Buttons>
    </View>
  );
};

const Home = () => {
  return (
    <View style={{ flex: 1 }}>
      <TopBar></TopBar>
      <BotBar></BotBar>
      <Buttons></Buttons>
    </View>
  );
};

const Search = () => {
  return (
    <View style={{ flex: 1 }}>
      <TopBar></TopBar>
      <BotBar></BotBar>
      <Buttons></Buttons>
    </View>
  );
};

//Bars and Buttons

const BotBar = () => {
  return (
    <View style={{ justifyContent: "flex-end", flex: 1 }}>
      <View style={barGraphic.botBorder} />
    </View>
  );
};

const TopBar = () => {
  return (
    <View style={{ justifyContent: "flex-start", flex: 1 }}>
      <View style={barGraphic.topBorder} />
    </View>
  );
};

const Buttons = () => {
  return (
    <View style={botButtons.layout}>
      <TouchableOpacity>
        <Image style={botButtons.size} source={require("./assets/home.png")} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Image style={botButtons.size} source={require("./assets/defter.png")} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Image style={botButtons.size} source={require("./assets/sepet.png")} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Image style={botButtons.size} source={require("./assets/ENV.png")} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Image style={botButtons.size} source={require("./assets/menü.png")} />
      </TouchableOpacity>
    </View>
  );
};

//StyleSheets

const botButtons = StyleSheet.create({
  size: {
    height: 60,
    width: 60,
  },
  layout: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-end",
  },
});

const barGraphic = StyleSheet.create({
  bars: {
    flex: 1,
  },
  topBorder: {
    borderColor: "#008E97",
    backgroundColor: "#008E97",
    borderBottomStartRadius: 45,
    borderBottomEndRadius: 45,
    borderWidth: 60,
  },
  botBorder: {
    borderColor: "#008E97",
    backgroundColor: "#008E97",
    borderTopStartRadius: 45,
    borderTopEndRadius: 45,
    borderWidth: 40,
  },
});

export default MenuNotLogged;

/*  onPress={.bind()} 

const Asdf = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("http://127.0.0.1:8000/category/")
      .then((response) => response.json())
      .then((json) => setData(json.results))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={{ flex: 1, padding: 24 }}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            
            <TouchableOpacity onPress={App.bind()}>{item.name}</TouchableOpacity>
          )}
        />
      )}
    </View>
  );
};


const App = () => {
  const [_isLoading, _setLoading] = useState(true);
  const [_data, _setData] = useState([]);

  useEffect(() => {
    
    fetch('http://127.0.0.1:8000/ingredients/')
      .then((response) => response.json())
      .then((json) => _setData(json.results))
      .catch((error) => console.error(error))
      .finally(() => _setLoading(false));
  }, []);

  return (
    <View style={{ flex: 1, padding: 24 }}>
      {_isLoading ? <ActivityIndicator/> : (
        <FlatList
          _data={_data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            
            <Text>{item.name}</Text>
          )}
        />
      )}
    </View>
  );
};
*/
